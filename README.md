# README - GitHelp

## What is Git?

**Git** allows groups of people to work on the same documents (often code) at the same time, and without stepping on each other's toes. It's a popular *distributed version control system*. So extremely popular that you can't ignore it.

## What is this repository for?

**GitHelp** does... what it says. My purpose is to help people using git.

## How?

I've compiled my favorite cheatsheets and books here as source files. If you have time, you can take a look at them. If you don't, I recommend you glance at the rest of this readme.

- - - -

On this page:

* [Some advice](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-some-advice)
* [Create and clone](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-create-and-clone)
* [Configure](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-configure)
* [Review often](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-review-often)
* [Stage and snapshot](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-stage-and-snapshot)
* [Synchronize](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-synchronize)
* [Branch and merge](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-branch-and-merge)
* [Undoing things](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-undoing-things)
* [Issues](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-issues)
    * [Deleted but staged](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-deleted-but-staged)
    * [Commit without name and email](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-commit-without-name-and-email)
    * [Push without commit](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-push-without-commit)
    * [Conflicts and mergetool](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-conflicts-and-mergetool)
    * [Fast-forward and rebase](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-fast-forward-and-rebase)
    * [Merge conflict](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-merge-conflict)
* [Documentation](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-documentation)
* [Coming next](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-coming-next)

- - - -

# Some advice

* Split tasks and commit as soon as you can
* Don't try to commit code that doesn't run
* Split independent tasks
* Use new branches for experimentation
* Always keep a stable branch
* Keep in touch with your coworkers and coordinate your actions
* Give understandable titles to your commits
* Keep up with possible changes from your coworkers and give your opinion


# Create and clone
aka: init or clone, create, setting up a repository

**create new** repository

`$ git init`

**clone** repository

`$ git clone https://axltlninja@bitbucket.org/axltlninja/githelp.git`


# Configure
aka: name, email, tool, color, untrack, gitignore, exclude from tracking

`$ git config user.name 'axltlninja'`

`$ git config user.email 'axltlninja+bitbucket@gmail.com'`

`--global`

`$ git config -l` list configurations

`$ git config -e` hard edit

**Optional**: enable colorization of command line output & merge tool

`$ git config --global color.ui auto`

`$ git config --global merge.tool meld` useful for pull --rebase conflict

**Untrack**: suppress versioning of files and paths matching the specified patterns:

    .gitignore
    ----------
    \*.log
    build/
    temp-\*

`$ git ls-files --other --ignored --exclude-standard` list ignored files


# Review often
aka: check, inspect, status, log, history, graph, diff, show, metadata, compare, who to blame

**modifications & conflicts**

`$ git status`

**compare**

`$ git diff` what is changed but not staged

`$ git diff --staged` what is staged but not yet committed

`$ git diff master featureA`

**history**

`$ git log`

`$ git log --oneline` just id & message

`$ git log -p` with detailled diff

`$ git log --stat` with compacted diff

`$ git log --summary` with added/deleted files

`$ git log --follow contributors.txt` for a file even beyond rename

**graph** for an illustrated history

`$ git log --graph --oneline`

**show commit** metadata

`$ git show ff161ab`

**blame** who changed file.txt

`$ git blame file.txt`


# Stage and snapshot
aka: stage, untrack, rm, delete, snapshot, reset, remove, commit, record

**stage** snapshot files for the next commit

`$ git add contributors.txt`

`$ git add .` everything in and beneath

`$ git add -A .` even file deletions

**Use quotes** when using wildcards: `$ git add '*.txt'`

**move** file and prepare for commit

`$ git mv here.txt there.txt`

**unstage** from stage only

`$ git reset contributors.txt`

`$ git rm --cached contributors.txt`

**remove** from stage and disk if not done

`$ git rm contributors.txt`

`$ git reset --hard contributors.txt`

**commit** save a snapshot locally

`$ git commit -m "Add cute octocat story"`

`$ git commit -am "Delete stuff"` auto remove deleted files with the commit


# Synchronize
aka: remote, push, fetch, pull, register an online repo, put your work online, upload, download

**register remote** repository

`$ git remote add origin 'https://axltlninja@bitbucket.org/axltlninja/githelp.git'`

**upload** changes to remote repository

`$ git push origin master`

**download** the remote repo to another branch

`$ git fetch origin`

**combine** changes directly from remote repo

`$ git pull`


# Branch and merge
aka: checkout, combine

**list** local branches

`$ git branch`

**create** new branch

`$ git branch featureA`

`$ git checkout -b featureA` create and switch

**switch** branch

`$ git checkout master`

**view** changes between branches

`$ git diff master featureA`

**combine** changes from another branch

`$ git merge featureA`

**push** branch to remote repository

`$ git push origin featureA`

**delete** branch 

`$ git branch -d featureA` the branch was merged and left unchanged

`$ git branch -D featureA` force delete


# Undoing things
aka: checkout, reset, undo, discard changes, re-edit

Remember, anything that is committed in Git can almost always be recovered. Even commits that were on branches that were deleted or commits that were overwritten with an --amend commit can be recovered.

**replace** last local commit and re-edit the message

`$ git commit --amend`

**file** restauration

`$ git checkout -- contributors.txt` lose modifications since last commit

`$ git checkout [commit] file` lose modifications since [commit]

**undo commits** after [commit]

`$ git reset [commit]` preserve changes locally

`$ git reset --hard [commit]` discard everything back to the commit

**HEAD** pointers

`HEAD` is the last commit of the current branch

`featureA/HEAD` is the last commit of the branch featureA

`featureA` points to the last commit of featureA

`HEAD^` points to the commit before HEAD

`HEAD^^` points to commits before HEAD

`HEAD~42` points to 42 commits before HEAD


# Issues


## Deleted but staged


## Commit without name and email
aka: can't commit because of bad configuration, check configuration

Check if name and email have been configured and hard edit if necessary. Be careful, because sometimes "user.email" is just mispelled "user.mail".

    $ git config user.name 'axltlninja'
    $ git config user.email 'axltlninja+bitbucket@gmail.com'
    $ git config -e


## Push without commit
aka: can't push because there's no commit, error: src refspec does not match any

This means you forgot to commit your new changes, so just commit. If there's still an error, check your git configuration.


## Conflicts and mergetool

Un `conflict` a lieu quand on cherche à mettre en commun des fichiers dont les mêmes sections ont été modifiées par deux personnes.

2 ways to solve a conflict: [manually](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-manually), [with mergetool](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-mergetool)

### manually

    $ git status                    # find conflicts
    $ vim files_with_conflicts.txt  # modified by git  
    <<<<<<< HEAD
    ancien contenu
    =======
    nouveau contenu
    >>>>>>> 
    $ git add .
    $ git commit -m

### mergetool

Le mergetool présente sur trois colonnes, trois versions du fichier:

* `local` à gauche: votre dernière version de la branche réceptrice,

* `remote` à droite: la dernière version de la branche à fusionner,

* `origin` au centre: la version commune, où porter les modifications.


`$ git mergetool`
`$ rm '*.origin'`

go to: [fast-forward and rebase](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-fast-forward-and-rebase), [merge conflict](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-merge-conflict)


## Fast-forward and rebase
aka: can't push a new commit because the remote repo has been modified

**fast-forward** push is rejected because of a risk of overwriting existing commits

**rebase** une opération qui consiste à appliquer nos commits à la suite de ceux des autres et peut engendrer des
`conflict`

    $ git pull --rebase       # dl last commit and rebase ours after
    $ git mergetool
    $ git status              # check conflicts
    $ rm '*.origin'
    $ git rebase --continue   # confirm rebase
    $ git push origin master
More about: [git mergetool](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-mergetool)


## Merge conflict
aka: can't merge branches because of conflict

    $ git mergetool
    $ git status                                      # check conflicts
    $ rm '*.origin'
    $ git add .
    $ git commit -m "Merge dans master de featureA"   # merge commit
    $ git push origin master
    $ git branch -d featureA

More about: [git mergetool](https://bitbucket.org/axltlninja/githelp/overview#markdown-header-mergetool)

# Documentation
aka: man, help, references, tutorials

**get help** about git commands

`$ git help rm`

`$ git rm --help`

`$ man git rm`

**references**

* [git manpages](https://git-htmldocs.googlecode.com/git/git.html)
* [Atlassian tutorial](https://www.atlassian.com/git/tutorials/setting-up-a-repository/)
* [ProGit](http://git-scm.com/book/en/v2)
* [GitHub git cheatsheet](https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf)
* [OverApi](http://overapi.com/git/)
* [Tower git cheatsheet](http://www.git-tower.com/blog/command-line-cheat-sheet-detail/)
* [Nina cheatsheet](https://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf)
* [Bitbucket tutorial repo](https://bitbucket.org/axltlninja/tutorial)

**help with markdown**

* [Bitbucket tutorial](https://bitbucket.org/tutorials/markdowndemo)

# Coming next

git stash, git stash apply, git push -u, pull request